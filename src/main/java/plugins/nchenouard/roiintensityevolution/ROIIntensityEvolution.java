package plugins.nchenouard.roiintensityevolution;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import icy.canvas.IcyCanvas;
import icy.gui.component.sequence.SequenceChooser;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.IcyFrameEvent;
import icy.gui.frame.IcyFrameListener;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.gui.main.GlobalROIListener;
import icy.gui.main.GlobalSequenceListener;
import icy.image.IcyBufferedImage;
import icy.main.Icy;
import icy.plugin.abstract_.PluginActionable;
import icy.roi.ROI;
import icy.roi.ROIEvent;
import icy.roi.ROIEvent.ROIEventType;
import icy.roi.ROIListener;
import icy.sequence.Sequence;
import icy.util.StringUtil;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 * Compute and display intensity evolution in ROIs
 *
 * @author Nicolas Chenouard. Distributed under the GPLv3 license.
 * @version 2.0 (08/02/2014)
 */

public class ROIIntensityEvolution extends PluginActionable
        implements IcyFrameListener, ROIListener, GlobalSequenceListener, GlobalROIListener
{
    IcyFrame mainFrame;
    JFreeChart chart;
    JPanel chartsPanel = new JPanel();
    JPanel mainPanel = new JPanel();

    final SequenceChooser sequenceSelector = new SequenceChooser(false, "Select image sequence here", 20);
    Sequence selectedSequence = null;

    final JPanel roiListPane = new JPanel();

    HashMap<ROI, JCheckBox> listedROIBoxList = new HashMap<>();
    HashMap<ROI, ROIAnalysis> roiSeries = new HashMap<>();

    HashMap<Sequence, IcyBufferedImage> projections = new HashMap<>();

    final static String[] criteria = new String[] {"Mean intensity", "Max intensity", "Min intensity",
            "Median instensity", "Intensity sum", "Intensity variance", "Over threshold pixel count"};
    JComboBox<String> criteriaBox = new JComboBox<>(criteria);

    JButton refreshButton = new JButton("Refresh display");
    JCheckBox autoRefreshBox = new JCheckBox("Auto refresh display");

    ArrayList<ROI> seriesToFill = new ArrayList<>();
    ReentrantLock seriesToFillLock = new ReentrantLock();
    Condition seriesToFillCondition = seriesToFillLock.newCondition();

    JCheckBox overLaySeriesBox = new JCheckBox("Overlay series:");

    ReentrantLock analyzedLock = new ReentrantLock();

    private FillSeriesThread fillSeriesThread;

    private ArrayList<Thread> savingThreadList = new ArrayList<>();

    boolean notPacked = true;

    double threshold = 0;
    boolean useRealScales = false;
    boolean pixelAboveThreshold = true;

    @Override
    public void run()
    {
        mainFrame = new IcyFrame("ROI Intensity Evolution", true, true, true, true);
        mainFrame.addFrameListener(this);
        mainFrame.setVisible(true);
        Icy.getMainInterface().addGlobalSequenceListener(this);
        Icy.getMainInterface().addGlobalROIListener(this);

        JMenuBar menuBar = new JMenuBar();
        JMenu exportMenu = new JMenu("Save");
        menuBar.add(exportMenu);
        JMenuItem exportItem = new JMenuItem("Save results to XLS file");
        exportMenu.add(exportItem);
        exportItem.addActionListener(arg0 -> exportResults());
        JMenu optionMenu = new JMenu("Options");
        menuBar.add(optionMenu);
        JMenuItem displaySeriesItem = new JMenuItem("Display extra series from SwimmingPool");
        optionMenu.add(displaySeriesItem);
        displaySeriesItem.addActionListener(e -> {
            IcyFrame optionFrame = new ExtraSeriesFrame();
            optionFrame.setVisible(true);
            addIcyFrame(optionFrame);
        });
        JMenuItem setThresholdItem = new JMenuItem("Set thresholding options");
        optionMenu.add(setThresholdItem);
        setThresholdItem.addActionListener(e -> {
            IcyFrame optionFrame = new ThresholdingOptionFrame();
            optionFrame.setVisible(true);
            addIcyFrame(optionFrame);
        });

        JMenu aboutMenu = new JMenu("About");
        menuBar.add(aboutMenu);
        JMenuItem manualItem = new JMenuItem("Manual");
        aboutMenu.add(manualItem);
        manualItem.addActionListener(e -> JOptionPane.showMessageDialog(mainPanel,
                "Please refer to the online help:\n http://icy.bioimageanalysis.org/plugin/ROI-Intensity-Evolution",
                "Manual", JOptionPane.INFORMATION_MESSAGE));
        JMenuItem aboutItem = new JMenuItem("About");
        aboutMenu.add(aboutItem);
        aboutItem.addActionListener(e -> JOptionPane.showMessageDialog(mainPanel,
                "This plugin is distributed under GPL v3 license.\n Author: Nicolas Chenouard", "About",
                JOptionPane.INFORMATION_MESSAGE));
        mainFrame.setJMenuBar(menuBar);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(chartsPanel, BorderLayout.CENTER);

        JPanel eastPanel = new JPanel(new BorderLayout());
        JPanel eastNorthPanel = new JPanel(new GridBagLayout());
        eastPanel.add(eastNorthPanel, BorderLayout.NORTH);
        GridBagConstraints eastC = new GridBagConstraints();
        eastC.fill = GridBagConstraints.HORIZONTAL;
        eastC.gridheight = 1;
        eastC.gridwidth = 1;
        eastC.gridx = 0;
        eastC.gridy = 0;

        eastNorthPanel.add(new JLabel("Sequence to analyse:"), eastC);
        eastC.gridy++;

        eastNorthPanel.add(sequenceSelector, eastC);
        eastC.gridy++;
        sequenceSelector.addActionListener(arg0 -> changeSelectedSequence(sequenceSelector.getSelectedSequence()));

        // eastNorthPanel.add(new JLabel("Display options:"), eastC);
        // eastC.gridy++;
        eastNorthPanel.add(refreshButton, eastC);
        eastC.gridy++;
        eastNorthPanel.add(autoRefreshBox, eastC);
        autoRefreshBox.setSelected(true);
        eastC.gridy++;
        refreshButton.addActionListener(arg0 -> refreshROIlist(selectedSequence));
        autoRefreshBox.addActionListener(arg0 -> {
            if (autoRefreshBox.isSelected())
                refreshROIlist(selectedSequence);
        });

        eastNorthPanel.add(new JLabel("Criterion to display:"), eastC);
        eastC.gridy++;
        eastNorthPanel.add(criteriaBox, eastC);
        eastC.gridy++;
        criteriaBox.addActionListener(e -> updateDisplay());
        eastNorthPanel.add(new JLabel("Regions of Interest:"), eastC);

        roiListPane.setLayout(new BoxLayout(roiListPane, BoxLayout.Y_AXIS));
        JScrollPane scrollRoiListPane = new JScrollPane(roiListPane);
        eastPanel.add(scrollRoiListPane, BorderLayout.CENTER);

        JPanel eastSouthPanel = new JPanel(new GridBagLayout());
        eastC.gridy = 0;
        eastC.weightx = 1;
        JButton selectAllButton = new JButton("Select all");
        selectAllButton.addActionListener(e -> selectAllROIs());
        eastSouthPanel.add(selectAllButton, eastC);
        eastC.gridy++;

        JButton unselectAllButton = new JButton("Unselect all");
        unselectAllButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                unselectAllROIs();
            }
        });
        eastSouthPanel.add(unselectAllButton, eastC);
        eastPanel.add(eastSouthPanel, BorderLayout.SOUTH);

        mainPanel.add(eastPanel, BorderLayout.EAST);

        mainFrame.setContentPane(mainPanel);
        // mainFrame.pack();
        mainFrame.addToDesktopPane();
        mainFrame.center();
        mainFrame.setVisible(true);
        mainFrame.requestFocus();

        fillSeriesThread = new FillSeriesThread();
        fillSeriesThread.start();
        SwingUtilities.invokeLater(() -> refreshROIlist(selectedSequence));
    }

    public static ArrayList<ROIAnalysis> analyseRoisInSequence(Sequence seq, boolean useRealScales, double threshold,
            boolean pixelAboveThreshold) throws InterruptedException
    {
        ArrayList<ROI> toFill = seq.getROIs();
        ArrayList<ROIAnalysis> analyzed = new ArrayList<ROIAnalysis>();
        for (ROI roi : toFill)
        {
            double scaling = 1;
            if (useRealScales)
                scaling = seq.getPixelSizeX() * seq.getPixelSizeY() * seq.getPixelSizeZ();
            ROIAnalysis analysis = new ROIAnalysis(roi, seq, roi.getShowName() + "#" + roi.getId(), threshold,
                    pixelAboveThreshold, scaling);
            analyzed.add(analysis);
        }
        return analyzed;
    }

    private void exportResults()
    {
        ArrayList<ROIAnalysis> analyzedTracks = new ArrayList<>();
        try
        {
            analyzedLock.lock();
            for (Entry<ROI, JCheckBox> entry : listedROIBoxList.entrySet())
            {
                if (entry.getValue().isSelected())
                {
                    analyzedTracks.add(roiSeries.get(entry.getKey()));
                }
            }
        }
        finally
        {
            analyzedLock.unlock();
        }
        exportToXLS(analyzedTracks);
    }

    public static void exportToXLS(final ArrayList<ROIAnalysis> trackList, final File XLSFile)
    {
        WritableWorkbook workbook = null;
        try
        {
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            workbook = Workbook.createWorkbook(XLSFile, wbSettings);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            new AnnounceFrame("Error creating XLS file. Resuls saving aborted!");
            return;
        }
        int numChannels = computeMaxNumChannels(trackList);
        try
        {
            int cntSheet = 0;
            for (int c = 0; c < numChannels; c++)
            {
                for (int i = 0; i < 7; i++)
                {
                    WritableSheet sheet = workbook.createSheet("Channel " + c + " - " + criteria[i], cntSheet);
                    cntSheet++;
                    // create the time axis
                    int minT = 0;
                    int maxT = 0;
                    int cnt = 0;
                    int maxNumCol = 200;
                    int cntSheet2 = 0;
                    for (ROIAnalysis trkAnalysis : trackList)
                    {
                        switch (i)
                        {
                            case 0:
                                if (trkAnalysis.getMeanIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getMeanIntensity()[c].getMaxX();
                                break;
                            case 1:
                                if (trkAnalysis.getMaxIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getMaxIntensity()[c].getMaxX();
                                break;
                            case 2:
                                if (trkAnalysis.getMinIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getMinIntensity()[c].getMaxX();
                                break;
                            case 3:
                                if (trkAnalysis.getMedianIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getMedianIntensity()[c].getMaxX();
                                break;
                            case 4:
                                if (trkAnalysis.getSumIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getSumIntensity()[c].getMaxX();
                                break;
                            case 5:
                                if (trkAnalysis.getVarIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getVarIntensity()[c].getMaxX();
                                break;
                            case 6:
                                if (trkAnalysis.getVarIntensity()[c].getMaxX() > maxT)
                                    maxT = (int) trkAnalysis.getROISize()[c].getMaxX();
                                break;

                        }
                    }
                    sheet.addCell(new Label(0, 0, "Frame number"));
                    for (int t = minT; t <= maxT; t++)
                        sheet.addCell(new Number(0, t - minT + 1, t));
                    int cntTrack = 0;
                    for (ROIAnalysis trkAnalysis : trackList)
                    {
                        if (cnt > maxNumCol)
                        {
                            cnt = 0;
                            cntSheet++;
                            cntSheet2++;
                            sheet = workbook.createSheet("Channel " + c + " - " + criteria[i] + "-" + cntSheet2,
                                    cntSheet);
                            sheet.addCell(new Label(0, 0, "Frame number"));
                            for (int t = minT; t <= maxT; t++)
                                sheet.addCell(new Number(0, t - minT + 1, t));
                            cntTrack = 0;
                        }
                        cnt++;
                        cntTrack++;
                        sheet.addCell(new Label(cntTrack, 0, trkAnalysis.getDescription()));
                        int firstT = 0;
                        switch (i)
                        {
                            case 0:
                            {
                                if (trkAnalysis.getMeanIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getMeanIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getMeanIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            }
                            case 1:
                                if (trkAnalysis.getMaxIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getMaxIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getMaxIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            case 2:
                                if (trkAnalysis.getMinIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getMinIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getMinIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            case 3:
                                if (trkAnalysis.getMedianIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getMedianIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getMedianIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            case 4:
                                if (trkAnalysis.getSumIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getSumIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getSumIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            case 5:
                                if (trkAnalysis.getVarIntensity().length >= c)
                                    for (int j = 0; j < trkAnalysis.getVarIntensity()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getVarIntensity()[c].getY(j).doubleValue()));
                                    }
                                break;
                            case 6:
                                if (trkAnalysis.getROISize().length >= c)
                                    for (int j = 0; j < trkAnalysis.getROISize()[c].getItemCount(); j++)
                                    {
                                        sheet.addCell(new Number(cntTrack, firstT + j + 1,
                                                trkAnalysis.getROISize()[c].getY(j).doubleValue()));
                                    }
                                break;

                        }
                    }
                }
            }
        }
        catch (jxl.write.WriteException e)
        {
            e.printStackTrace();
        }
        try
        {
            workbook.write();
            workbook.close();
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void exportToXLS(final ArrayList<ROIAnalysis> trackList)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose xls file.");
        int returnVal = chooser.showOpenDialog(this.mainPanel);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;

        File file = chooser.getSelectedFile();
        if (file == null)
            return;
        if (!file.getName().endsWith(".xls"))
        {
            try
            {
                file = new File(file.getCanonicalPath() + ".xls");
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        final File XLSFile = file;

        Thread savingThread = new Thread()
        {
            @Override
            public void run()
            {
                AnnounceFrame announce1 = new AnnounceFrame("Saving results");
                exportToXLS(trackList, XLSFile);
                savingThreadList.remove(this);
                announce1.close();
                new AnnounceFrame("Results saved");
            }
        };
        savingThreadList.add(savingThread);
        savingThread.start();
    }

    private void selectAllROIs()
    {
        for (JCheckBox box : listedROIBoxList.values())
            box.setSelected(true);
        refreshSelectedList();
    }

    private void unselectAllROIs()
    {
        for (JCheckBox box : listedROIBoxList.values())
            box.setSelected(false);
        updateDisplay();
    }

    private void changeSelectedSequence(Sequence selectedSequence)
    {
        Sequence oldSequence = this.selectedSequence;
        this.selectedSequence = selectedSequence;
        refreshROIlist(this.selectedSequence);
        if (oldSequence != selectedSequence)
            updateDisplay();
    }

    private void refreshROIlist(final Sequence sequence)
    {
        SwingUtilities.invokeLater(() -> {
            final List<ROI> toRemove = new ArrayList<>();
            final List<ROI> toAdd = new ArrayList<>();
            final List<ROI> rois = new ArrayList<>();

            // important to do all the process in the EDT
            // otherwise we could end up adding / removing ROIs several time
            if (sequence != null)
                rois.addAll(sequence.getROIs());

            for (ROI roi : rois)
                if (!listedROIBoxList.containsKey(roi))
                    toAdd.add(roi);

            for (Entry<ROI, JCheckBox> e : listedROIBoxList.entrySet())
                if (!rois.contains(e.getKey()))
                    toRemove.add(e.getKey());

            // add new ROI
            for (ROI roi : toRemove)
            {
                roiListPane.remove(listedROIBoxList.get(roi));
                listedROIBoxList.remove(roi);
            }
            // remove old ROI
            for (ROI roi : toAdd)
            {
                JCheckBox box = new JCheckBox(roi.getName());
                box.addActionListener(e -> refreshSelectedList());
                roiListPane.add(box);
                listedROIBoxList.put(roi, box);
                roi.addListener(ROIIntensityEvolution.this);
            }
            roiListPane.updateUI();
            refreshSelectedList();
        });
    }

    private void refreshSelectedList()
    {
        refreshAllResults(selectedSequence);
    }

    private ArrayList<ROI> getToAnalyzeTracks()
    {
        ArrayList<ROI> toAnalyze = new ArrayList<>();
        ArrayList<ROI> toFillCopy = new ArrayList<>();
        ArrayList<ROI> analyzedROIs = new ArrayList<>();
        try
        {
            seriesToFillLock.lock();
            toFillCopy.addAll(seriesToFill);
        }
        finally
        {
            seriesToFillLock.unlock();
        }
        try
        {
            analyzedLock.lock();
            analyzedROIs.addAll(roiSeries.keySet());
        }
        finally
        {
            analyzedLock.unlock();
        }
        for (Entry<ROI, JCheckBox> entry : listedROIBoxList.entrySet())
        {
            if (entry.getValue().isSelected())
            {
                if (!toFillCopy.contains(entry.getKey()))
                    toAnalyze.add(entry.getKey());
            }
        }
        return toAnalyze;
    }

    public void updateDisplay()
    {
        ArrayList<ROIAnalysis> analyzedROIList = new ArrayList<>();
        if (selectedSequence != null)
        {
            analyzedLock.lock();
            try
            {
                for (Entry<ROI, JCheckBox> e : listedROIBoxList.entrySet())
                {
                    if (e.getValue().isSelected() && roiSeries.get(e.getKey()) != null)
                        analyzedROIList.add(roiSeries.get(e.getKey()));
                }
            }
            finally
            {
                analyzedLock.unlock();
            }
        }
        updateCharts(analyzedROIList);
    }

    private static int computeMaxNumChannels(ArrayList<ROIAnalysis> analyzedROIList)
    {
        int maxNumChannels = 0;
        for (ROIAnalysis tr : analyzedROIList)
        {
            if (tr != null)
                if (tr.getNumChannels() > maxNumChannels)
                    maxNumChannels = tr.getNumChannels();
        }
        return maxNumChannels;
    }

    private void updateCharts(ArrayList<ROIAnalysis> analyzedROIList)
    {
        int maxNumChannels = computeMaxNumChannels(analyzedROIList);
        chartsPanel.removeAll();

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

        for (int i = 0; i < analyzedROIList.size(); i++)
        {
            renderer.setSeriesStroke(i, new BasicStroke(2f));
            renderer.setSeriesShapesVisible(i, false);
        }

        if (maxNumChannels > 0)
            chartsPanel.setLayout(new GridLayout(maxNumChannels, 1));
        else
        {
            // chartsPanel.setLayout(new GridLayout(1, 1));
            chartsPanel.setLayout(new BoxLayout(chartsPanel, BoxLayout.PAGE_AXIS));
            XYSeriesCollection xyDataset = new XYSeriesCollection();

            if (seriesToOverlay != null)
                xyDataset.addSeries(seriesToOverlay);

            String TitleString = "ROI Intensity Evolution";
            String TitleString2 = "Time";
            String TitleString3 = "Intensity";
            boolean displayLegend = true;

            chart = ChartFactory.createXYLineChart(TitleString, TitleString2, TitleString3, xyDataset,
                    PlotOrientation.VERTICAL, displayLegend, true, false);

            int minWidth = 300;
            int minHeight = 200;
            int width = 300;
            int height = 200;
            int maxWidth = 100000;
            int maxHeight = 100000;

            ChartPanel panel = new ChartPanel(chart, width, height, minWidth, minHeight, maxWidth, maxHeight, false,
                    true, true, true, true, true);
            panel.setMinimumSize(new Dimension(width, height));
            panel.setSize(new Dimension(width, height));
            chart.getPlot().setBackgroundPaint(new Color(230, 230, 230));
            chart.getXYPlot().setRenderer(renderer);
            chart.getXYPlot().setRangeGridlinePaint(Color.BLACK);
            chart.getXYPlot().setDomainGridlinePaint(Color.BLACK);
            // chart.getPlot().setOutlineStroke(new BasicStroke(2));
            chartsPanel.add(panel);
        }

        for (int c = 0; c < maxNumChannels; c++)
        {
            XYSeriesCollection xyDataset = new XYSeriesCollection();
            String TitleString = "";

            if (selectedSequence != null)
            {
                TitleString = selectedSequence.getChannelName(c);
            }
            else
            {
                TitleString = "ch" + c;
            }

            if (seriesToOverlay != null)
                xyDataset.addSeries(seriesToOverlay);

            String TitleString3 = "";
            for (ROIAnalysis analysis : analyzedROIList)
            {
                XYSeries cropSeries = null;
                switch (criteriaBox.getSelectedIndex())
                {
                    case 0:
                        if (analysis.getMeanIntensity().length >= c)
                            cropSeries = analysis.getMeanIntensity()[c];// can be null here
                        TitleString3 = "Mean Intensity";
                        break;
                    case 1:
                        if (analysis.getMaxIntensity().length >= c)
                            cropSeries = analysis.getMaxIntensity()[c];
                        TitleString3 = "Max Intensity";
                        break;
                    case 2:
                        if (analysis.getMinIntensity().length >= c)
                            cropSeries = analysis.getMinIntensity()[c];
                        TitleString3 = "Min Intensity";
                        break;
                    case 3:
                        if (analysis.getMedianIntensity().length >= c)
                            cropSeries = analysis.getMedianIntensity()[c];
                        TitleString3 = "Median Intensity";
                        break;
                    case 4:
                        if (analysis.getSumIntensity().length >= c)
                            cropSeries = analysis.getSumIntensity()[c];
                        TitleString3 = "Intensity sum";
                        break;
                    case 5:
                        if (analysis.getVarIntensity().length >= c)
                            cropSeries = analysis.getVarIntensity()[c];
                        TitleString3 = "Intensity variance";
                        break;
                    case 6:
                        if (analysis.getROISize().length >= c)
                            cropSeries = analysis.getROISize()[c];
                        if (pixelAboveThreshold)
                            TitleString3 = "Pixel area over threshold";
                        else
                            TitleString3 = "Pixel area below threshold";
                        break;
                }
                xyDataset.addSeries(cropSeries);
            }

            String TitleString2 = "Time";
            boolean displayLegend = true;

            chart = ChartFactory.createXYLineChart(TitleString, TitleString2, TitleString3, xyDataset,
                    PlotOrientation.VERTICAL, displayLegend, true, false);

            int minWidth = 300;
            int minHeight = 200;
            int width = 300;
            int height = 200;
            int maxWidth = 100000;
            int maxHeight = 100000;

            chart.getPlot().setBackgroundPaint(new Color(230, 230, 230));
            chart.getXYPlot().setRenderer(renderer);
            chart.getXYPlot().setRangeGridlinePaint(Color.BLACK);
            chart.getXYPlot().setDomainGridlinePaint(Color.BLACK);
            // chart.getPlot().setOutlineStroke(new BasicStroke(2));
            chartsPanel.add(new ChartPanel(chart, width, height, minWidth, minHeight, maxWidth, maxHeight, false, false,
                    true, true, true, true));
        }

        if (notPacked)
        {
            notPacked = false;
            mainFrame.pack();
        }

        chartsPanel.validate();
        chartsPanel.repaint();
        mainFrame.repaint();
    }

    public void refreshAllResults(Sequence sequence)
    {
        try
        {
            seriesToFillLock.lock();
            analyzedLock.lock();
            seriesToFill.clear();
            roiSeries.clear();
        }
        finally
        {
            analyzedLock.unlock();
            seriesToFillLock.unlock();
        }
        ArrayList<ROI> toFill = getToAnalyzeTracks();
        try
        {
            seriesToFillLock.lock();
            seriesToFill.addAll(toFill);
            seriesToFillCondition.signalAll();
        }
        finally
        {
            seriesToFillLock.unlock();
        }
    }

    @Override
    public void icyFrameIconified(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameDeiconified(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameActivated(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameDeactivated(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameInternalized(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameExternalized(IcyFrameEvent e)
    {
    }

    // ROIListener methods
    @Override
    public void roiChanged(ROIEvent event)
    {
        // ROI name changed ? --> update name in list
        if ((event.getType() == ROIEventType.PROPERTY_CHANGED)
                && StringUtil.equals(event.getPropertyName(), ROI.PROPERTY_NAME))
        {
            // update name on checkbox
            for (Entry<ROI, JCheckBox> e : listedROIBoxList.entrySet())
                if (e.getKey() == event.getSource())
                    e.getValue().setText(event.getSource().getName());
        }

        // no need to recompute if ROI itself didn't changed
        if (event.getType() != ROIEventType.ROI_CHANGED)
            return;

        if (autoRefreshBox.isSelected())
        {
            ROI roi = event.getSource();
            if (roi != null && roi.getSequences().contains(selectedSequence))
            {
                if (event.getType() == ROIEventType.ROI_CHANGED)
                {
                    ROIAnalysis analysis = roiSeries.get(roi);
                    if (analysis != null)
                    {
                        try
                        {
                            seriesToFillLock.lock();
                            if (!seriesToFill.contains(roi))
                                seriesToFill.add(roi);
                            seriesToFillCondition.signalAll();
                        }
                        finally
                        {
                            seriesToFillLock.unlock();
                        }
                    }
                }
                if (event.getType() == ROIEventType.FOCUS_CHANGED)
                {
                    if (roi.isSelected())
                    {
                        for (Entry<ROI, JCheckBox> entry : listedROIBoxList.entrySet())
                        {
                            if (entry.getKey() == roi)
                            {
                                entry.getValue().setSelected(true);
                                refreshSelectedList();
                            }
                        }
                    }
                }
            }
        }

    }

    class FillSeriesThread extends Thread
    {
        boolean stop = false;

        @Override
        public void run()
        {
            while (!stop)
            {
                ArrayList<ROI> toFill = new ArrayList<>();
                try
                {
                    seriesToFillLock.lock();
                    if (seriesToFill.isEmpty())
                        seriesToFillCondition.await();
                    toFill.addAll(seriesToFill);
                    seriesToFill.clear();

                    // fill the series
                    if (selectedSequence != null)
                        fillSeries(toFill, selectedSequence);
                    else
                        SwingUtilities.invokeLater(ROIIntensityEvolution.this::updateDisplay);
                }
                catch (InterruptedException e)
                {
                    new FailedAnnounceFrame("ROI intensity evolution process interrupted..");
                }
                finally
                {
                    seriesToFillLock.unlock();
                }
            }
        }
    }

    void fillSeries(ArrayList<ROI> toFill, Sequence selectedSequence) throws InterruptedException
    {
        if (toFill.isEmpty())
            return;
        ArrayList<ROIAnalysis> analyzed = new ArrayList<>();
        for (ROI roi : toFill)
        {
            double scaling = 1;
            if (useRealScales)
                scaling = selectedSequence.getPixelSizeX() * selectedSequence.getPixelSizeY();
            ROIAnalysis analysis = new ROIAnalysis(roi, selectedSequence, roi.getName() + "#" + roi.getId(), threshold,
                    pixelAboveThreshold, scaling);
            analyzed.add(analysis);
        }
        try
        {
            analyzedLock.lock();
            for (ROIAnalysis analysis : analyzed)
                roiSeries.put(analysis.getROI(), analysis);
        }
        finally
        {
            analyzedLock.unlock();
        }

        SwingUtilities.invokeLater(this::updateDisplay);
    }

    double convertScale(IcyCanvas canvas, double value)
    {
        return canvas.canvasToImageLogDeltaX((int) value);
    }

    @Override
    public void sequenceOpened(Sequence sequence)
    {
    }

    @Override
    public void sequenceClosed(Sequence sequence)
    {
        if (sequence == selectedSequence)
        {
            for (ROI roi : selectedSequence.getROIs())
                roi.removeListener(this);
        }

        selectedSequence = null;
    }

    @Override
    public void icyFrameOpened(IcyFrameEvent e)
    {
    }

    @Override
    public void icyFrameClosing(IcyFrameEvent e)
    {
        Icy.getMainInterface().removeGlobalSequenceListener(this);
        for (ROI roi : listedROIBoxList.keySet())
            roi.removeListener(this);
    }

    @Override
    public void icyFrameClosed(IcyFrameEvent e)
    {
        Icy.getMainInterface().removeGlobalSequenceListener(this);
        for (ROI roi : listedROIBoxList.keySet())
            roi.removeListener(this);
    }

    @Override
    public void roiAdded(ROI roi)
    {
        refreshROIlist(selectedSequence);
    }

    @Override
    public void roiRemoved(ROI roi)
    {
        refreshROIlist(selectedSequence);
    }

    class ThresholdingOptionFrame extends IcyFrame
    {
        NumberFormat numberFormat;
        JFormattedTextField thresholdField = new JFormattedTextField(numberFormat);
        JCheckBox countPixelAboveThreshold = new JCheckBox("Count pixel above threshold.");
        JCheckBox useRealScalesBox = new JCheckBox("Use real area scales instead of pixel count.");
        JButton setValuesButton = new JButton("Set values");

        protected ThresholdingOptionFrame()
        {
            super("Thresholding options");

            JPanel mainPanel = new JPanel(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();
            gc.fill = GridBagConstraints.HORIZONTAL;
            gc.gridy = 0;

            this.setContentPane(mainPanel);
            JTextArea descriptionArea = new JTextArea(
                    "In each selected ROI the number of pixels that or above, or below,\n a user-defined threshold for pixel value can be counted.");
            mainPanel.add(descriptionArea, gc);
            gc.gridy++;

            mainPanel.add(new JLabel("Pixel threshold value:"), gc);
            gc.gridy++;

            mainPanel.add(thresholdField, gc);
            gc.gridy++;
            thresholdField.setValue(threshold);
            mainPanel.add(countPixelAboveThreshold, gc);
            gc.gridy++;
            countPixelAboveThreshold.setSelected(pixelAboveThreshold);
            countPixelAboveThreshold.setSelected(pixelAboveThreshold);
            mainPanel.add(useRealScalesBox, gc);
            gc.gridy++;
            useRealScalesBox.setSelected(useRealScales);
            mainPanel.add(setValuesButton, gc);
            gc.gridy++;
            setValuesButton.addActionListener(arg0 -> {
                threshold = ((java.lang.Number) thresholdField.getValue()).doubleValue();
                pixelAboveThreshold = countPixelAboveThreshold.isSelected();
                useRealScales = useRealScalesBox.isSelected();
                refreshSelectedList();
                ThresholdingOptionFrame.this.close();
            });
            this.pack();
        }
    }

    XYSeries seriesToOverlay = null;

    public void changeSeriesToOverlay(XYSeries series)
    {
        if (series != seriesToOverlay)
        {
            updateDisplay();
        }
    }

    class ExtraSeriesFrame extends IcyFrame
    {
        SwimmingObjectBox<XYSeries> seriesBox = new SwimmingObjectBox<>(XYSeries.class);

        protected ExtraSeriesFrame()
        {
            super("Extra series to display");
            JPanel mainPanel = new JPanel(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();
            gc.fill = GridBagConstraints.HORIZONTAL;
            gc.gridy = 0;
            gc.weightx = 1;
            gc.gridwidth = 2;
            this.setContentPane(mainPanel);

            mainPanel.add(new JLabel("Select below a temporal series in the SwimmingPool to be overlayed:"), gc);
            gc.gridy++;

            mainPanel.add(seriesBox, gc);
            gc.gridy++;

            JButton displaySeriesButton = new JButton("Overlay series");
            gc.weightx = 0.5;
            gc.gridx = 0;
            gc.gridwidth = 1;

            mainPanel.add(displaySeriesButton, gc);
            displaySeriesButton.addActionListener(arg0 -> {
                changeSeriesToOverlay((XYSeries) seriesBox.getSelectedItem());
                ExtraSeriesFrame.this.close();
            });
            gc.gridx++;

            JButton cancelButton = new JButton("No series to overlay");
            cancelButton.addActionListener(e -> {
                changeSeriesToOverlay(null);
                ExtraSeriesFrame.this.close();
            });
            mainPanel.add(cancelButton, gc);

            this.pack();
        }

    }
}