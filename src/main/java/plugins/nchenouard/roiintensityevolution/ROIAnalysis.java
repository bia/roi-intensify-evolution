package plugins.nchenouard.roiintensityevolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.jfree.data.xy.XYSeries;

import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.sequence.Sequence;
import icy.type.collection.array.ArrayUtil;

/**
 * Object to manage intensity values of a ROI through time
 * 
 * @version 2.0 (08/02/2014)
 * @author Nicolas Chenouard. Distributed under the GPLv3 license.
 */

public class ROIAnalysis
{
    private ROI roi;
    private Sequence sequence;
    private String description;

    private final XYSeries[] meanIntensity;
    private final XYSeries[] minIntensity;
    private final XYSeries[] maxIntensity;
    private final XYSeries[] medianIntensity;
    private final XYSeries[] sumIntensity;
    private final XYSeries[] varIntensity;
    private final XYSeries[] roiSize;

    ReentrantLock initLock = new ReentrantLock();

    public ROIAnalysis(ROI roi, Sequence sequence, String description, double threshold, boolean overthreshold,
            double scaling) throws InterruptedException
    {
        try
        {
            initLock.lock();
            if (roi == null)
                throw new IllegalArgumentException(
                        "NULL roi object is not allowed for creating a TrackAnalysis instance");
            if (sequence == null)
                throw new IllegalArgumentException(
                        "NULL Sequence object is not allowed for creating a TrackAnalysis instance");
            if (description == null)
                throw new IllegalArgumentException(
                        "NULL description object is not allowed for creating a TrackAnalysis instance");
            this.roi = roi;
            this.sequence = sequence;
            this.description = description;

            meanIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                meanIntensity[c] = new XYSeries(description);
            minIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                minIntensity[c] = new XYSeries(description);
            maxIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                maxIntensity[c] = new XYSeries(description);
            medianIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                medianIntensity[c] = new XYSeries(description);
            sumIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                sumIntensity[c] = new XYSeries(description);
            varIntensity = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                varIntensity[c] = new XYSeries(description);
            roiSize = new XYSeries[sequence.getSizeC()];
            for (int c = 0; c < sequence.getSizeC(); c++)
                roiSize[c] = new XYSeries(description);
            fillSeriesNoLock(threshold, overthreshold, scaling);
        }
        finally
        {
            initLock.unlock();
        }
    };

    private void fillSeriesNoLock(double threshold, boolean overthreshold, double areaScale) throws InterruptedException
    {
        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            this.meanIntensity[c].clear();
            this.maxIntensity[c].clear();
            this.minIntensity[c].clear();
            this.varIntensity[c].clear();
            this.sumIntensity[c].clear();
            this.medianIntensity[c].clear();
        }

        final BooleanMask2D[] masks = new BooleanMask2D[sequence.getSizeZ()];

        // 2D ROI ?
        if (roi instanceof ROI2D)
        {
            final ROI2D roi2d = (ROI2D) roi;
            final BooleanMask2D mask = roi2d.getBooleanMask(true);

            // add mask for each Z slice where ROI is active
            for (int z = 0; z < masks.length; z++)
                if (roi2d.isActiveFor(z, -1, -1))
                    masks[z] = mask;
        }
        // 3D ROI ?
        else if (roi instanceof ROI3D)
        {
            // add specific mask for each Z slice
            for (int z = 0; z < sequence.getSizeZ(); z++)
                masks[z] = (((ROI3D) roi).getBooleanMask2D(z, true));
        }
        // not supported
        else
        {
            for (int t = 0; t < sequence.getSizeT(); t++)
            {
                for (int c = 0; c < sequence.getSizeC(); c++)
                {
                    this.meanIntensity[c].add(t, 0);
                    this.maxIntensity[c].add(t, 0);
                    this.minIntensity[c].add(t, 0);
                    this.varIntensity[c].add(t, 0);
                    this.sumIntensity[c].add(t, 0);
                    this.medianIntensity[c].add(t, 0);
                    this.roiSize[c].add(t, 0);
                }
            }

            return;
        }

        for (int t = 0; t < sequence.getSizeT(); t++)
        {
            for (int c = 0; c < sequence.getSizeC(); c++)
            {
                final List<Double> valueList = new ArrayList<>();
                double sum = 0;
                double min = 0;
                double max = 0;
                double sumSq = 0;
                int cntPix = 0;

                for (int z = 0; z < sequence.getSizeZ(); z++)
                {
                    final BooleanMask2D mask2d = masks[z];

                    // empty mask here ? --> continue to next Z
                    if ((mask2d == null) || mask2d.isEmpty())
                        continue;

                    final IcyBufferedImage image = sequence.getImage(t, z);
                    final IcyBufferedImage subImage = IcyBufferedImageUtil.getSubImage(image, mask2d.bounds);
                    final boolean[] mask = mask2d.mask;
                    final double[] data = (double[]) ArrayUtil.arrayToDoubleArray(subImage.getDataXY(c),
                            image.isSignedDataType());

                    for (int off = 0; off < data.length; off++)
                    {
                        // pixel contained in ROI ?
                        if (mask[off])
                        {
                            final double value = data[off];

                            sum += value;
                            sumSq += value * value;
                            valueList.add(new Double(value));
                            if (value < min)
                                min = value;
                            if (value > max)
                                max = value;
                            if (overthreshold)
                            {
                                if (value > threshold)
                                    cntPix++;
                            }
                            else
                            {
                                if (value < threshold)
                                    cntPix++;
                            }
                        }
                    }
                }

                final int cntValues = valueList.size();
                double mean = 0;
                double var = 0;

                if (cntValues > 0)
                {
                    mean = sum / cntValues;
                    var = (sumSq / cntValues) - (mean * mean);
                }

                meanIntensity[c].add(t, mean);
                maxIntensity[c].add(t, max);
                minIntensity[c].add(t, min);
                varIntensity[c].add(t, var);
                sumIntensity[c].add(t, sum);

                if (cntValues > 0)
                {
                    // sort the list
                    Collections.sort(valueList);
                    if (cntValues == 1)
                        this.medianIntensity[c].add(t, valueList.get(0));
                    else if (cntValues % 2 == 0)
                    {
                        this.medianIntensity[c].add(t,
                                (valueList.get((int) cntValues / 2)) + (valueList.get(((int) cntValues / 2)) - 1) / 2);
                    }
                    else
                        this.medianIntensity[c].add(t, valueList.get((int) cntValues / 2));
                }

                this.roiSize[c].add(t, cntPix * areaScale);
            }
        }
    }

    public void fillSeries(double threshold, boolean overthreshold, double scale) throws InterruptedException
    {
        try
        {
            initLock.lock();
            fillSeriesNoLock(threshold, overthreshold, scale);
        }
        finally
        {
            initLock.unlock();
        }
    }

    public int getNumChannels()
    {
        try
        {
            initLock.lock();
            return sequence.getSizeC();
        }
        finally
        {
            initLock.unlock();
        }
    }

    public ROI getROI()
    {
        try
        {
            initLock.lock();
            return roi;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getMeanIntensity()
    {
        try
        {
            initLock.lock();
            return meanIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getMedianIntensity()
    {
        try
        {
            initLock.lock();
            return medianIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getSumIntensity()
    {
        try
        {
            initLock.lock();
            return sumIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getVarIntensity()
    {
        try
        {
            initLock.lock();
            return varIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getMinIntensity()
    {
        try
        {
            initLock.lock();
            return minIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getMaxIntensity()
    {
        try
        {
            initLock.lock();
            return maxIntensity;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public XYSeries[] getROISize()
    {
        try
        {
            initLock.lock();
            return roiSize;
        }
        finally
        {
            initLock.unlock();
        }
    }

    public String getDescription()
    {
        try
        {
            initLock.lock();
            return description;
        }
        finally
        {
            initLock.unlock();
        }
    }
}