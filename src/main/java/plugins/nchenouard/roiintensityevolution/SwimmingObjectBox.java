package plugins.nchenouard.roiintensityevolution;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Vector;

import icy.main.Icy;
import icy.swimmingPool.SwimmingObject;
import icy.swimmingPool.SwimmingPoolEvent;
import icy.swimmingPool.SwimmingPoolListener;
import icy.util.StringUtil;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

/**
 *  Select and display SwimmingObject objects of a given type from the SwimmingPool in a auto-refreshing JCombobox
 *  
 * @param <T> the class of the objects to select in the swimming pool
 * 
 * @version 1.0 November 21, 2012
 * 
 * @author Nicolas Chenouard. Distributed under the GPLv3 license.
 */


public class SwimmingObjectBox<T> extends JComboBox implements SwimmingPoolListener
{

	private static final long serialVersionUID = 1594001236878708868L;

	private final Class<T> itemClass;
	
	public SwimmingObjectBox(Class<T> itemClass)
	{
		this(itemClass, 50, "No valid object to display in SwimmingPool");
	}
	
	public SwimmingObjectBox(Class<T> itemClass, final int maxSize, final String defaultMessage)
	{
		super();
		this.itemClass = itemClass;
		this.listeners = new ArrayList<SwimmingObjectListener<T>>();
		
		Icy.getMainInterface().getSwimmingPool().addListener( this );
		this.setRenderer( new ListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
			{
				if (value == null)
					return new JLabel(defaultMessage);
				if (value instanceof SwimmingObject )
				{
					JLabel label = new JLabel(StringUtil.limit( ((SwimmingObject)value).getName() , maxSize ));
					label.setToolTipText(((SwimmingObject)value).getName());
					return label;
				}
				else
					return  new JLabel(value.toString());
			}
		});
	}

	public T getSelectedObject()
	{
		Object o = getSelectedItem();
		if (o != null && o instanceof SwimmingObject)
		{
			Object oo = ((SwimmingObject)o).getObject();
			if (oo.getClass() == itemClass)
				return itemClass.cast(oo);
			else
				return null;
		}
		else
			return null;
	}


	@Override
	public void swimmingPoolChangeEvent( final SwimmingPoolEvent event) {
		SwingUtilities.invokeLater( 

				new Runnable() {
					@Override
					public void run() {

						refreshList();

						// Select the last entry computed
						if ( event.getResult().getObject().getClass() == itemClass)
						{
							setSelectedItem( event.getResult().getObject() );
						}
					}
				}
				);
	}

	void refreshList()
	{
		// save old selection
		final Object oldSelected = getSelectedItem();
		// rebuild model
		setModel( new DefaultComboBoxModel( getSwimmingObjectList() ) );
		// restore selection
		setSelectedItem(oldSelected);
	}

	Vector<SwimmingObject> getSwimmingObjectList()
	{
		Vector<SwimmingObject> objectList = new Vector<SwimmingObject>();

		ArrayList<SwimmingObject> objects = Icy.getMainInterface().getSwimmingPool().getObjects();

		for ( SwimmingObject so : objects )
		{
			Object o = so.getObject();
			if ( o.getClass() == itemClass)
			{
				objectList.add(so);
			}
		}
		return objectList;
	}
	
	private ArrayList<SwimmingObjectListener<T>> listeners;

	public void addListener(SwimmingObjectListener<T> listener)
	{
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(SwimmingObjectListener<T> listener)
	{
		listeners.remove(listener);
	}
	
	@Override
	public void fireItemStateChanged(ItemEvent e)
	{
		for (SwimmingObjectListener<T> listener:listeners)
		{
			listener.swimmingObjectChanged(getSelectedObject());
		}
		super.fireItemStateChanged(e);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		for (SwimmingObjectListener<T> listener:listeners)
		{
			listener.swimmingObjectChanged(getSelectedObject());
		}
		super.actionPerformed(e);
	}

	public interface SwimmingObjectListener<T>
	{
		public void swimmingObjectChanged(T object);
	}
}